﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfCore
{
    public static class Extentions
    {
        public static string Sub(this string city)
        {
            return city.Substring(0, city.IndexOf(","));
        }

        public static string Translate(this string main)
        {
            switch (main)
            {
                case "Thunderstorm":
                    return "Буря";
                case "Drizzle":
                    return "Мелкий дождь";
                case "Rain":
                    return "Дождь";
                case "Snow":
                    return "Снег";
                case "Mist":
                    return "Туман";
                case "Clear":
                    return "Ясно";
                case "Clouds":
                    return "Облачно";
            }
            return "Ураган";
        }

    }
}
