﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace WpfCore.Model
{
    public class HourWeather : INotifyPropertyChanged
    {
        private string city;
        private string min;
        private string max;
        private string status;
        private string wind;
        private string humidity;
        private string date;
        private string icon;

        public string SelectedCity
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("SelectedCity");
            }
        }
        public string Min
        {
            get { return min; }
            set
            {
                min = value;
                OnPropertyChanged("Min");
            }
        }
        public string Max
        {
            get { return max; }
            set
            {
                max = value;
                OnPropertyChanged("Max");
            }
        }
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }
        public string RuStatus { get { return status.Translate(); } }
        public string Wind
        {
            get { return wind; }
            set
            {
                wind = value;
                OnPropertyChanged("Wind");
            }
        }
        public string Humidity
        {
            get { return humidity; }
            set
            {
                humidity = value;
                OnPropertyChanged("Humidity");
            }
        }
        public string Date
        {
            get { return date.Substring(date.Length-8,5); }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }
        public string FullDate { get { return date; } }
        public string Icon
        {
            get { return icon; }
            set
            {
                icon = "pack://application:,,,/images/icons/" + value + ".png";
                OnPropertyChanged("Icon");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
