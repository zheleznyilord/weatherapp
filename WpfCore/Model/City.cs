﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using WpfCore.Command;

namespace WpfCore.Model
{
    public class City : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private string country;
        private float lon;
        private float lat;
        
        public int Id
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("name");
            }
        }
        public string Country
        {
            get { return country; }
            set
            {
                country = value;
                OnPropertyChanged("country");
            }
        }
        public float Lon
        {
            get { return lon; }
            set
            {
                lon = value;
                OnPropertyChanged("Lon");
            }
        }
        public float Lat
        {
            get { return lat; }
            set
            {
                lat = value;
                OnPropertyChanged("Lat");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public override string ToString()
        {
            return name + ", " + country;
        }
    }
}
