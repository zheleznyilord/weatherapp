﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace WpfCore.Model
{
    public class DayWeather : INotifyPropertyChanged
    {
        private string city;
        private string temperature;
        private string status;
        private string wind;
        private string humidity;
        private string date;
        private string icon;
        private string description;
        private string sunrise;
        private string sundown;
        private string feel;
        private string pressure;
        private string figure;
        private float windDeg;
        private string windFigure;

        public ObservableCollection<HourWeather> Hours { get; set; }


        public float WindDeg
        {
            get { return windDeg; }
            set
            {
                windDeg = value;
                OnPropertyChanged("WindDeg");
            }
        }

        public string WindFigure
        {
            get { return windFigure; }
            set
            {
                windFigure = value;
                OnPropertyChanged("WindFigure");
            }
        }
        public string Figure
        {
            get { return figure; }
            set
            {

                figure = string.Format("M 0 0 l 40 0 a 40 40 0 1 0 {0} Z",value);
                OnPropertyChanged("Figure");
            }
        }
        public string Pressure
        {
            get { return pressure; }
            set
            {
                pressure = value;
                OnPropertyChanged("pressure");
            }
        }
        public string Feel
        {
            get { return feel; }
            set
            {
                feel = value;
                OnPropertyChanged("Feel");
            }
        }
        public string Sunrise
        {
            get { return sunrise; }
            set
            {
                sunrise = value;
                OnPropertyChanged("Sunrise");
            }
        }
        public string Sundown
        {
            get { return sundown; }
            set
            {
                sundown = value;
                OnPropertyChanged("Sundown");
            }
        }
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                addDescription();
                OnPropertyChanged("Description");
            }
        }
        public string SelectedCity
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("SelectedCity");
            }
        }
        public string Temperature
        {
            get { return temperature; }
            set
            {
                temperature = value;
                OnPropertyChanged("Temperature");
            }
        }
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }
        public string RuStatus { get { return status.Translate(); } }
        public string Wind
        {
            get { return wind; }
            set
            {
                wind = value;
                OnPropertyChanged("Wind");
            }
        }
        public string Humidity
        {
            get { return humidity; }
            set
            {
                humidity = value;
                OnPropertyChanged("Humidity");
            }
        }
        public string Date
        {
            get { return date.Substring(date.Length-2) + "." + date.Substring(date.Length-5,2); }
            set
            {
                date = value;
                OnPropertyChanged("Date");
            }
        }
        public string FullDate { get { return date; } }
        public string DayDate { get { return getDateDay(date.Substring(date.Length - 2)); } }
        public string Icon
        {
            get { return icon; }
            set
            {
                icon = "pack://application:,,,/images/icons/" + value + ".png";
                OnPropertyChanged("Icon");
            }
        }
        


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }


        private string getDateDay(string d)
        {
            var now = DateTime.Now;
            now = now.AddDays(long.Parse(d) - now.Day);
            var day = now.ToString("dddd");
            if (day.Substring(2, 1).Equals("о") || day.Substring(2, 1).Equals("е"))
            {
                return day.Substring(0, 1).ToUpper() + day.Substring(1, 1) + " " + d;
            }
            return  day.Substring(0,1).ToUpper() + day.Substring(2,1) + " " + d;

        }

        private void addDescription()
        {
            if (description == null)
            {
                description = RuStatus;
            }
            description = "В этот день " + description.ToLower() + ".";
            if (int.Parse(wind.Substring(0,2))>6)
            {
                description = description + " Будет ветренно.";
            }
            if (int.Parse(temperature.Substring(0, temperature.IndexOf(" "))) >15)
            {
                description = description + "Максимальная температура - " + Temperature +".";
            }
        }

        
    }
}
