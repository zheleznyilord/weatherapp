﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WpfCore.Entity;
using WpfCore.Model;
using WpfCore.ViewModel;

namespace WpfCore
{
    public class WeatherService
    {
        private const float FARENHEIT_COEF = (float)273.15;
        private const int FULL_DEGREE = 360;
        private const int PERCENT = 100;
        private const int RADIUS = 40;
        private const int COEF_ARROW = 15;

        public List<City> ParseCitiesJson(string filename)
        {
            var list = JsonConvert.DeserializeObject<List<City>>(File.ReadAllText(filename));

            return list;
        }

        public List<WeatherDayDTO> PerformDto(WeatherResponse response)
        {

            var group = response.List.GroupBy(l => l.Dt_txt.Substring(0, 10));
            var result = new List<WeatherDayDTO>();
            foreach (var item in group)
            {
                var items = item.ToList();
                var dto = new WeatherDayDTO
                {
                    Date = item.Key
                };
                foreach (var time in items)
                {
                    dto.Max += (int)(time.Main.Temp_max - FARENHEIT_COEF);
                    dto.Min += (int)(time.Main.Temp_min - FARENHEIT_COEF);
                }
                dto.Max /= items.Count;
                dto.Min /= items.Count;
                dto.Status = item.First().Weather.First().Main;
                result.Add(dto);
            }
            return result;
        }

        public DayViewModel GetViewModelByResponse(WeatherResponse response, string city)
        {
            var group = response.List.GroupBy(l => l.Dt_txt.Substring(0, 10));
            var listDto = new List<WeatherFullDTO>();
            var listDay = new List<DayWeather>();
            foreach (var item in group)
            {
                var items = item.ToList();
                var day = new DayWeather();
                var dto = new WeatherDayDTO
                {
                    Date = item.Key
                };
                day.Date = item.Key;
                int feel = 0;
                foreach (var time in items)
                {
                    dto.Max += (int)(time.Main.Temp_max - FARENHEIT_COEF);
                    dto.Min += (int)(time.Main.Temp_min - FARENHEIT_COEF);
                    feel += (int)(time.Main.Feels_like - FARENHEIT_COEF);
                }
                dto.Max /= items.Count;
                dto.Min /= items.Count;
                day.Feel = (feel / items.Count) + " C";
                day.Temperature = ((dto.Max - dto.Min) / 2 + dto.Min) + " C";
                dto.Status = item.First().Weather.First().Main;
                day.Status = item.First().Weather.First().Main;
                day.Humidity = item.First().Main.Humidity + "%";
                day.Figure = calculatePointsForCircle(item.First().Main.Humidity);
                day.WindDeg = item.First().Wind.Deg;
                day.WindFigure = calculateWindDirection(day.WindDeg);
                day.Wind = (int)item.First().Wind.Speed + " km/h";
                day.SelectedCity = city;
                day.Icon = items.First().Weather.First().Icon;
                day.Hours = new System.Collections.ObjectModel.ObservableCollection<HourWeather>(GetDetailDayHour(day.FullDate, response));
                day.Description = item.First().Weather.First().Descriptor;
                day.Sunrise = calculateSunrise();
                day.Sundown = calculateSundown();
                day.Pressure = items.First().Main.Pressure + " мм";
                listDay.Add(day);
            }
            var vm = new DayViewModel();
            vm.Days = new System.Collections.ObjectModel.ObservableCollection<DayWeather>(listDay);
            vm.SelectedDay = vm.Days.First();
            return vm;
        }

        private string calculateWindDirection(float deg)
        {
            double ex;
            double ey;
            double sx;
            double sy;
            
            if (deg < COEF_ARROW)
            {
                sx = getX(deg - COEF_ARROW);
                sy = getY(deg - COEF_ARROW);
            }
            else
            {
                sx = getX(FULL_DEGREE + deg - COEF_ARROW);
                sy = getY(FULL_DEGREE + deg - COEF_ARROW);
            }
            if (deg > (FULL_DEGREE - COEF_ARROW))
            {
                ex = getX(deg + COEF_ARROW - FULL_DEGREE);
                ey = getY(deg + COEF_ARROW - FULL_DEGREE);
            }
            else
            {
                ex = getX(deg + COEF_ARROW);
                ey = getY(deg + COEF_ARROW);
            }
            return string.Format("M 0 0 l {0} {1} a 40 40 0 1 0 {2} {3} Z", addSignX(sy, deg - COEF_ARROW), addSignY(sx, deg - COEF_ARROW), 
                addSignX(sy, ey, deg + COEF_ARROW), addSignY(sx, ex, deg + COEF_ARROW));
        }

        private int addSignY(double sy, double ey, float deg)
        {
            int y;
            if (deg<=90)
            {
                y = -(int)(sy - ey);
            } else if (deg<=180)
            {
                y = (int) (sy + ey);
            } else if (deg<=270)
            {
                y = - (int)(sy - ey);
            }
            else
            {
                y = (int)(sy + ey);
            }
            return (int)y;
        }

        private int addSignX(double sy, double ey, float deg)
        {
            int y;
            if (deg <= 90)
            {
                y = (int)(sy - ey);
            }
            else if (deg <= 180)
            {
                y = -(int)(sy + ey);
            }
            else if (deg <= 270)
            {
                y = -(int)(sy - ey);
            }
            else
            {
                y = (int)(sy + ey);
            }
            return (int)y;
        }

        private int addSignX(double x, float deg)
        {
            if (deg >= 90 || deg<=270)
            {
                x = -x;
            }
            return (int) x;
        }
        private int addSignY(double y, float deg)
        {
            if (deg >= 180)
            {
                y = - Math.Abs(y);
            }
            return (int) y;
        }

        private double getX(float angle)
        {
            var radAngle = angle * Math.PI / (FULL_DEGREE / 2);
            var cos = Math.Cos(radAngle);
            return Math.Abs(RADIUS * cos);
        }

        private double getY(float angle)
        {
            var radAngle = angle * Math.PI / (FULL_DEGREE / 2);
            var sin = Math.Sin(radAngle);
            return Math.Abs(RADIUS * sin);
        }

        private string calculatePointsForCircle(float humidity)
        {
            var angle = FULL_DEGREE * humidity / PERCENT;
            var x = getX(angle);
            var y = getY(angle);
            if (humidity <= 25)
            {
                x = -40 + x;
            }
            else if (humidity <= 50)
            {
                x = -x - RADIUS;

            }
            else if (humidity <= 75)
            {
                x = -x - RADIUS;
                y = -y;
            }
            else
            {
                x = -40 + x;
                y = -y;
            }
            return (int)x + " " + (int)y;
        }

        private string calculateSundown()
        {
            Random rnd = new Random();
            return rnd.Next(16, 23) + ":" + rnd.Next(0, 6) + rnd.Next(0, 6);
        }

        private string calculateSunrise()
        {
            Random rnd = new Random();
            return rnd.Next(5, 9) + ":" + rnd.Next(0, 6) + rnd.Next(0, 6);
        }

        public List<HourWeather> GetDetailDayHour(string date, WeatherResponse weatherResponse)
        {
            var group = weatherResponse.List.GroupBy(l => l.Dt_txt.Substring(0, 10));
            var one = group.Where(g => g.Key.Equals(date)).First();
            var list = one.ToList();
            var listDto = new List<HourWeather>();
            foreach (var weather in list)
            {
                var dto = new HourWeather();
                dto.Date = weather.Dt_txt;
                dto.Max = (int)(weather.Main.Temp_max - FARENHEIT_COEF) + " C";
                dto.Min = (int)(weather.Main.Temp_min - FARENHEIT_COEF) + " C";
                dto.Status = weather.Weather.First().Main;
                dto.Wind = weather.Wind.Speed.ToString();
                dto.Humidity = weather.Main.Humidity.ToString() + "%";
                dto.Icon = weather.Weather.First().Icon;
                listDto.Add(dto);
            }
            return listDto;
        }

        public List<WeatherFullDTO> GetDetailDay(string date, WeatherResponse weatherResponse)
        {
            var group = weatherResponse.List.GroupBy(l => l.Dt_txt.Substring(0, 10));
            var one = group.Where(g => g.Key.Equals(date)).First();
            var list = one.ToList();
            var listDto = new List<WeatherFullDTO>();
            foreach (var weather in list)
            {
                var dto = new WeatherFullDTO();
                dto.Date = weather.Dt_txt;
                dto.Max = (int)weather.Main.Temp_max;
                dto.Min = (int)weather.Main.Temp_min;
                dto.Status = weather.Weather.First().Main;
                dto.Wind = weather.Wind.Speed.ToString();
                dto.Humidity = weather.Main.Humidity.ToString() + "%";

                listDto.Add(dto);
            }
            return listDto;
        }

        public WeatherResponse CheckWeather(string cityName)
        {
            var client = new HttpClient();

            var url = "http://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "&appid=68dcfcfc5c3ca87ddf144cec9e1b86b2";

            var result = Task.Run(async () => await client.GetAsync(url));
            var res = result.Result;
            var status = res.StatusCode;
            var mes = Task.Run(async () => await res.Content.ReadAsStringAsync());
            var mesr = mes.Result;

            return JsonConvert.DeserializeObject<WeatherResponse>(mesr);
        }

        public string GetTemperatureNow(WeatherResponse weatherResponse)
        {
            var now = DateTime.Now;

            foreach (var d in weatherResponse.List)
            {
                var date = DateTime.Parse(d.Dt_txt);
                var minus = now.Hour - date.Hour;
                if (now.Day.Equals(date.Day) && minus <= 2 && minus >= -2)
                {
                    return d.Main.Temp + " K";
                }
            }
            return "";
        }
    }
}