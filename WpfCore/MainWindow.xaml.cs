﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfCore.Entity;
using WpfCore.Model;
using WpfCore.ViewModel;

namespace WpfCore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WeatherService _service;
        CityViewModel Model;

        public MainWindow()
        {
            InitializeComponent();
            _service = new WeatherService();

            var model = new CityViewModel();
            model.All = _service.ParseCitiesJson("city.list.json");
            model.Cities = new ObservableCollection<City>(model.All.Take(5).ToList());
            Model = model;
            DataContext = model;

            string style = "light";
            // определяем путь к файлу ресурсов
            var urii = new Uri(style + ".xaml", UriKind.Relative);
            // загружаем словарь ресурсов
            ResourceDictionary resourceDict = Application.LoadComponent(urii) as ResourceDictionary;
            // очищаем коллекцию ресурсов приложения
            Application.Current.Resources.Clear();
            // добавляем загруженный словарь ресурсов
            Application.Current.Resources.MergedDictionaries.Add(resourceDict);

        }

        //private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    var box = (ComboBox)sender;
        //    List<City> list;
        //    if (Model.SelectedCity == null || !box.Text.Equals(Model.SelectedCity.ToString()))
        //    {
        //        if (box.Text.Contains(","))
        //        {
        //            list = Model.All.Where(x => x.Name.Contains(box.Text.Substring(0, box.Text.IndexOf(",")))).ToList();
        //        }
        //        else
        //        {
        //            list = Model.All.Where(x => x.Name.Contains(box.Text)).ToList();
        //        }
        //        Model.Cities.Clear();
        //        foreach (var city in list.Take(5).ToList())
        //        {
        //            Model.Cities.Insert(0, city);
        //        }
        //    }
        //}

        //private void ClickCity(object sender, RoutedEventArgs e)
        //{
        //    WeatherWindow weather = new WeatherWindow();
        //    if (Box.SelectedItem != null)
        //    {
        //        weather.SelectItem(Box.SelectedItem.ToString(),Model);
        //        weather.Show();
        //        this.Close();
        //    }
        //}


    }
}
