﻿namespace WpfCore.Entity
{
    public class Codes
    {
        public int Id { get; set; }
        public string Main { get; set; }
        public string Descriptor { get; set; }
        public string Icon { get; set; }
    }
}