﻿using System;
using System.Collections.Generic;
using System.Text;
using WpfCore.Model;

namespace WpfCore.Entity
{
    public class WeatherResponse
    {
        public string Cod { get; set; }
        public int Message { get; set; }
        public int Cnt { get; set; }
        public List<WeatherList> List { get; set; }
        public City City { get; set; }
    }
}
