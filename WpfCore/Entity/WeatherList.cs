﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfCore.Entity
{
    public class WeatherList
    {
        public long Dt { get; set; }
        public Temperature Main { get; set; }
        public List<Codes> Weather { get; set; }
        public Cloud Clouds { get; set; }
        public Wind Wind { get; set; }
        public Rain Sys { get; set; }
        public string Dt_txt { get; set; }

    }
}
