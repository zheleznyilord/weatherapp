﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfCore.Entity
{
    public class WeatherFullDTO
    {
        public string Date { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Status { get; set; }
        public string Humidity { get; set; }
        public string Wind { get; set; }
        

    }
}
