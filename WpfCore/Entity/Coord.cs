﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfCore.Entity
{
    public class Coord
    {
        public float Lon { get; set; }
        public float Lat { get; set; }
    }
}
