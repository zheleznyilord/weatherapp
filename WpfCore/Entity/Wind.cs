﻿namespace WpfCore.Entity
{
    public class Wind
    {
        public float Speed { get; set; }
        public float Deg { get; set; }
    }
}