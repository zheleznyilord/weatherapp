﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfCore.Entity
{
    public class WeatherDayDTO
    {
        public string Date { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Status { get; set; }
    }
}
