﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfCore.Entity;
using WpfCore.Model;
using WpfCore.ViewModel;

namespace WpfCore
{

    public partial class WeatherWindow : Window
    {
        WeatherService _service;
        DayViewModel DayViewModel;
        private string style = "light";
        private delegate void NoArgDelegate();
        private delegate void TwoArgDelegate(string arg, CityViewModel model);

        public string Selected { get; set; }
        public WeatherResponse Response { get; set; }
        public WeatherWindow()
        {
            InitializeComponent();
            _service = new WeatherService();
            DataContext = new DayViewModel();
        }

        public void SelectItem(string city, CityViewModel model)
        {
            Selected = city;
            var thread = new Thread(()=>AddContext(city,model));
            thread.Start();

        }

        public void AddContext(string city, CityViewModel model)
        {
            Response = _service.CheckWeather(city.Sub());
            DayViewModel day = _service.GetViewModelByResponse(Response, city);
            day.City = model;
            this.Dispatcher.BeginInvoke(new Action(()=> {
                DayViewModel = day;
                DataContext = day;
            }));
        }

        private void RefreshClick(object sender, RoutedEventArgs e)
        {
            SelectItem(Selected, DayViewModel.City);
        }

        

        private void Loop(object sender, RoutedEventArgs e)
        {
            if (!Box.IsVisible)
            {
                Box.Visibility = Visibility.Visible;
            }
            else
            {
                Box.Visibility = Visibility.Collapsed;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Hide(object sender, RoutedEventArgs e)
        {
            if (Refresh.IsVisible)
            {
                Refresh.Visibility = Visibility.Collapsed;
                Theme.Visibility = Visibility.Collapsed;
            }
            else
            {
                Refresh.Visibility = Visibility.Visible;
                Theme.Visibility = Visibility.Visible;
            }

        }

        private void ThemeClick(object sender, RoutedEventArgs e)
        {
            string now = "light";
            if (style.Equals(now))
            {
                style = "dark";
            } else
            {
                style = now;
            }
            // определяем путь к файлу ресурсов
            var urii = new Uri(style + ".xaml", UriKind.Relative);
            // загружаем словарь ресурсов
            ResourceDictionary resourceDict = Application.LoadComponent(urii) as ResourceDictionary;
            // очищаем коллекцию ресурсов приложения
            Application.Current.Resources.Clear();
            // добавляем загруженный словарь ресурсов
            Application.Current.Resources.MergedDictionaries.Add(resourceDict);
        }

        private void Box_Selected(object sender, RoutedEventArgs e)
        {
            var obj = (ComboBox)sender;
            if (obj.SelectedValue!=null)
            {
                SelectItem(obj.SelectedValue.ToString(), DayViewModel.City);
            }

        }
    }
}
