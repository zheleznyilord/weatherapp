﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Media;
using WpfCore.Model;

namespace WpfCore.ViewModel
{
    public class DayViewModel : INotifyPropertyChanged
    {
        private DayWeather selectedDay;
        private string back;
        private CityViewModel city;
        public ObservableCollection<DayWeather> Days { get; set; }

        public CityViewModel City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }

        public DayWeather SelectedDay
        {
            get { return selectedDay; }
            set
            {
                selectedDay = value;
                Back = selectBack(value);
                OnPropertyChanged("SelectedDay");
            }
        }
        public string Back
        {
            get { return back; }
            set
            {
                back = value;
                OnPropertyChanged("Back");
            }
        }

        public DayViewModel()
        {
            Days = new ObservableCollection<DayWeather> { new DayWeather { Date = "2222", Temperature = "2312" } };
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private string selectBack(DayWeather day)
        {
            if (day.Status.Equals("clear sky") || day.Status.Equals("few clouds"))
            {
                return "pack://application:,,,/images/sun.jpg";
            }
            else if (day.Status.Contains("ain"))
            {
                return "pack://application:,,,/images/rain.jpg";
            }
            else if (day.Status.Contains("now"))
            {
                return "pack://application:,,,/images/snow.jpg";
            }
            return "pack://application:,,,/images/cloudess.jpg";
        }
    }
}
