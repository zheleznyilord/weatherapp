﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using WpfCore.Command;
using WpfCore.Model;

namespace WpfCore.ViewModel
{
    public class CityViewModel : INotifyPropertyChanged
    {

        private City selectedCity;

        public ObservableCollection<City> Cities { get; set; }
        public List<City> All { get; set; }

        private string text;

        public string Text
        {
            get { return text; }
            set
            {
                text = value;

                List<City> list;
                if (SelectedCity == null || !text.Equals(SelectedCity.ToString()))
                {
                    if (text.Contains(","))
                    {
                        list = All.Where(x => x.Name.Contains(text.Substring(0, text.IndexOf(",")))).ToList();
                    }
                    else
                    {
                        list = All.Where(x => x.Name.Contains(text)).ToList();
                    }
                    Cities.Clear();
                    foreach (var city in list.Take(5).ToList())
                    {
                        Cities.Insert(0, city);
                    }
                }
                OnPropertyChanged("Text");
            }
        }

        private CityCommand chooseCity;
        public CityCommand ChooseCity
        {
            get 
            {
                return chooseCity ??
                    (chooseCity = new CityCommand(obj =>
                    {
                        var city = obj as MainWindow;
                    if (city != null && SelectedCity!=null)
                        {
                            WeatherWindow weather = new WeatherWindow();
                            weather.SelectItem(SelectedCity.ToString(), this);
                            weather.Show();
                            city.Close();
                        }
                    },
                    (obj) => obj != null));
            }

        }
        private CityCommand selectCity;
        public CityCommand SelectCity
        {
            get
            {
                return selectCity ??
                    (chooseCity = new CityCommand(obj =>
                    {
                        if (SelectedCity != null)
                        {
                            //weather.SelectItem(SelectedCity.ToString(), this);
                        }
                    }));
            }

        }

        public City SelectedCity
        {
            get { return selectedCity; }
            set
            {
                selectedCity = value;
                OnPropertyChanged("SelectedCity");
            }
        }

        public CityViewModel()
        {

        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

    }
}
